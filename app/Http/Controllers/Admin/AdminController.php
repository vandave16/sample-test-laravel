<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Employee;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function employeeList(){

        $getEmployee = Employee::get();

        return view('admin.employeelist', compact('getEmployee'));
    }

    public function editEmployee($id){
        $employee = Employee::findorfail($id);

        return view('admin.editEmployee', compact('employee'));
    }

    public function addEmployee(){
        $user = \Auth::user();

        return view('admin.addEmployee');
    }

    public function updateEmployee(Request $request){
        $user = \Auth::user();

        return view('admin.addEmployee');
    }

    public function deleteEmployee($id){
        Employee::find($id)->delete();

        return redirect()->route('admin-employee-list');
    }

}