@extends('layouts.admin')

@section('content')
<div class="page-header">
      <h1 class="page-title">Employee List</h1>
</div>
<div class="page-content">
    <div class="panel">
        <div class="panel-body">
        <div class="row row-lg">
                <div class="col-md-12">
                <!-- Example #exampleTableditToolbars -->
                    <div class="example-wrap">
                        <div class="example table-responsive">                            
                            <table id="admin_employee_list" class="table table-striped table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">First Name</th>
                                      <th scope="col">Last Name</th>
                                      <th scope="col">Contact</th>
                                      <th scope="col">Email Address</th>
                                      <th scope="col"></th>
                                    </tr>
                                  </thead>
                                <tbody>
                                    @foreach($getEmployee as $key => $emp)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $emp->firstname }}</td>
                                            <td>{{ $emp->lastname }}</td>
                                            <td>{{ $emp->phone_number }}</td>
                                            <td>{{ $emp->email }}</td>
                                            <td class="text-center" width="100px">
                                                <a href="{{ route('admin-employee-edit',$emp->id) }}"><i class="fa fa-edit"></i></a>
                                                <a href="{{ route('admin-employee-delete',$emp->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End Example #exampleTableditToolbars -->
                </div>
            </div> 
        </div>
    </div>
</div>

@endsection
