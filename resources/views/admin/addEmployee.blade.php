@extends('layouts.admin')

@section('content')
<div class="page-header">
      <h1 class="page-title">Add Employee</h1>
</div>
<div class="page-content">
    <div class="panel">
        <div class="panel-heading">
            <form id="addemployee" method="POST" action="{{ route('admin-employee-save') }}" class="form-horizontal" style="padding: 0px 30px;">
                @csrf

                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">First Name: </label>
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name" autocomplete="off">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Last Name: </label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name"  autocomplete="off">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Gender: </label>
                        <select name="gender" class="form-control">
                                <option></option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Birthday: </label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name"  autocomplete="off">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Age: </label>
                        <select name="gender" class="form-control">
                                <option></option>
                                @for($c=1; $c<=100; $c++)
                                <option value="{{ $c }}">{{ $c }}</option>
                                @endfor
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Address: </label>
                        <textarea name="address" id="address" class="form-control" placeholder="Address"  autocomplete="off">

                        </textarea> 
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Phone Number: </label>
                        <input type="text" class="form-control" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number"  autocomplete="off">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="form-control-label">Email Address </label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="test@email.com"  autocomplete="off">
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="button" id="saveemployee" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        <div class="panel-body">
                {{ \Auth::user()['type'] }}
        </div>
    </div>
</div>
@endsection
