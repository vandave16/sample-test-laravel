@extends('layouts.admin')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

@section('content')
<div class="page-header">
      <h1 class="page-title">Edit Employee</h1>
</div>
<div class="page-content">
    <div class="panel">
        <div class="panel-heading">
            <form id="editEmployee" method="POST" action="{{ route('admin-employee-update') }}" class="form-horizontal" style="padding: 0px 30px;">
                @csrf
                    <div class="form-group">
                        <label class="form-control-label">First Name: </label>
                        <input type="text" class="form-control" name="firstname" id="firstname" value="{{ $employee->firstname }}" placeholder="First Name" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Last Name: </label>
                        <input type="text" class="form-control" name="lastname" id="lastname" value="{{ $employee->lastname }}" placeholder="Last Name"  autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Gender: </label>
                        <select name="gender" class="form-control">
                                <option></option>
                                <option value="M"@if($employee->gender=='M'){{ 'selected' }}@endif>Male</option>
                                <option value="F"@if($employee->gender=='F'){{ 'selected' }}@endif>Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Birthday: </label>
                        <input type="text" class="form-control datepicker" name="birthday" value="{{ $employee->birthday }}" placeholder="Last Name"  autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Age: </label>
                        <select name="age" class="form-control">
                                <option></option>                                
                                @php
                                    $selected='';
                                @endphp
                                
                                @for($c=1; $c<=100; $c++)
                                <option value="{{ $c }}"@if($employee->age==$c){{ 'selected' }}@endif>{{ $c }}</option>
                                @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Address: </label>
                        <textarea name="address" id="address" class="form-control" placeholder="Address">{{ $employee->address }}</textarea> 
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Phone Number: </label>
                        <input type="text" class="form-control" class="form-control" name="phone_number" value="{{ $employee->phone_number }}" placeholder="Phone Number"  autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Email Address </label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="test@email.com" value="{{ $employee->email }}" autocomplete="off">
                    </div>

                <div class="form-group text-right">
                    <input type="submit" id="updateemployee" value="Update" class="btn btn-primary">
                </div>
            </form>
        </div>
        <div class="panel-body">
                {{ \Auth::user()['type'] }}
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
         });
</script>
@endsection
