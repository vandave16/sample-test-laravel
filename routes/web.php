<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::post('/main/checklogin', 'MainController@checklogin')->name('login-check');
Route::get('main/logout', 'MainController@logout')->name('logout');

Route::get('/admin/employee/edit/{id}', 'Admin\AdminController@editEmployee')->name('admin-employee-edit');
Route::get('/admin/employee/delete/{id}', 'Admin\AdminController@deleteEmployee')->name('admin-employee-delete');
Route::post('/admin/employee/update', 'Admin\AdminController@updateEmployee')->name('admin-employee-update');
Route::get('/admin/employee/list', 'Admin\AdminController@employeeList')->name('admin-employee-list');
Route::get('/admin/employee/add', 'Admin\AdminController@addEmployee')->name('admin-employee-add');
Route::post('/admin/employee/save', 'Admin\AdminController@saveEmployee')->name('admin-employee-save');
