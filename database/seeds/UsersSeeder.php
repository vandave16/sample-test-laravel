<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([ 
            'firstname' => 'Admin',
            'lastname' => 'Test',
            'username' => 'admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin123')
        ]);

        User::create([ 
            'firstname' => 'Van Dave',
            'lastname' => 'Decio',
            'username' => 'vandave',
            'email' => 'vandave.decio@gmail.com',
            'password' => Hash::make('password1')
        ]);
    }
}
