<?php

use Illuminate\Database\Seeder;
use App\Employee;
use Illuminate\Support\Str;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([ 
            'firstname' => 'Van Dave',
            'lastname' => 'Decio',
            'gender' => 'M',
            'birthday' => '1990-10-16',
            'age' => '29',
            'address' => 'UA7 Pristine Grove Residences, Purok 2 San Jose, Cebu City, Philippines, 6000',
            'phone_number' => '56889588',
            'email' => 'vandave.decio@gmail.com'
        ]);
        foreach (range(1,5) as $index) {
            Employee::create([ 
                'firstname' => Str::random(30),
                'lastname' => Str::random(30),
                'gender' => 'M',
                'birthday' => '1990-01-01',
                'age' => '29',
                'address' => 'Test Address '.Str::random(30),
                'phone_number' => '56889588',
                'email' => Str::random(10).'@gmail.com'
            ]);
        }
    }
}
